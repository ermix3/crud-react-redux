import * as types from '../types'


// Posts actions
export const add_post = post => ({type:types.ADD_POST , payload: post})
export const remove_post = id => ({type:types.REMOVE_POST , payload: id})
export const update_post = post => ({type:types.UPDATE_POST , payload: post})