import {combineReducers, legacy_createStore} from "redux"
import {post} from "./reducers"

const rootReducer = combineReducers({post})
const store = legacy_createStore(rootReducer)
export default store