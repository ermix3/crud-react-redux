import * as types from '../types/index.jsx'


// AddPost reducer
const initState = {
    posts: [
        {id: "1", title: "post1", body: "bla bla"},
        {id: "2", title: "post2", body: "bla bla"},
        {id: "3", title: "post3", body: "bla bla"},
    ]
}


export const post = (state = initState, action) => {
    switch (action.type) {
        case types.ADD_POST:
            return {...state, posts: [...state.posts, action.payload]}
        case types.REMOVE_POST:
            return {...state, posts: [...state.posts.filter(post => post.id !== action.payload)]}
        case types.UPDATE_POST:
            return {
                ...state,
                posts: [...state.posts.map(post => post.id === action.payload.id ? action.payload : post)]
            }
        default:
            return state
    }
}

