import {useState} from "react";
import {connect} from "react-redux";
import {useNavigate, useParams} from "react-router-dom";

import {update_post} from "../../redux/actions";

const Edit = ({posts, updatePost}) => {
    window.document.title = "Edit Post"
    const {id} = useParams()
    const oldPost = posts.find(post => post.id === id)
    const [post, setPost] = useState(oldPost)

    const handleChange = ({target}) => setPost({...post, [target.name]: target.value})

    const navigate = useNavigate()

    const handleSubmit = e => {
        e.preventDefault()
        updatePost({...post})

        navigate('/posts')
    }

    return (
        <>
            <h1>Edit Post</h1>
            {post && <div style={{"display": "flex"}}>
                <form onSubmit={handleSubmit}>
                    <input type="text"
                           name="title"
                           value={post.title}
                           onChange={handleChange}/><br/>

                    <textarea cols={20}
                              rows={10}
                              name="body"
                              value={post.body}
                              onChange={handleChange}/><br/>

                    <input type="submit" value="Update Post"/>
                </form>
            </div>}
        </>)
}

const mapStateToProps = state => ({posts: state.post.posts})
const mapDispatchToProps = dispatch => ({updatePost: post => dispatch(update_post(post))})

export default connect(mapStateToProps, mapDispatchToProps)(Edit)