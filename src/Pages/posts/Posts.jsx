import {useState} from "react"
import {connect} from "react-redux"
import {Link} from "react-router-dom";
import * as actions from "../../redux/actions/index.jsx";

const Posts = ({posts, removePost}) => {
    window.document.title = "All Post"

    const [post, setPost] = useState({id: '', title: '', body: ''})

    const handleChange = ({target}) => setPost({...post, [target.name]: target.value})

    return (
        <>
            <h1>Add Post</h1>
            <div>
                <section style={{'display':'flex'}}>
                    {
                        posts.length ? (posts.map((post, index) => <div key={post.id} style={{'border':'3px solid #fff', 'margin':'5px 10px', 'padding':'7px 11px'}}>
                            <h2>{post.title}</h2>
                            <p>{post.body}</p>
                            <button onClick={() => removePost(post.id)}>Delete</button>
                            <button style={{'margin':'0 10px'}}><Link to={`/posts/edit/${post.id}`}>Edit</Link></button>
                            <button><Link to={`/posts/${post.id}`}>Detail</Link></button>
                        </div>)) : (<h1>No Posts</h1>)
                    }
                </section>
            </div>
        </>
    )
}

const mapStateToProps = state => ({posts: state.post.posts})
const mapDispatchToProps = dispatch => ({removePost: id => dispatch(actions.remove_post(id))})

export default connect(mapStateToProps, mapDispatchToProps)(Posts)