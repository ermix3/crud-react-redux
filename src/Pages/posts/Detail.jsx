import {connect} from "react-redux";
import {Link, useParams} from "react-router-dom";

const Detail = ({posts}) => {
    window.document.title = "Detail Post"

    const {id} = useParams()
    const post = posts.find(post => post.id === id)

    return (
        <>
            <h1>Detail Post</h1>
            {post && <article>
                <h2>{post.title}</h2>
                <p>{post.body}</p>
                <Link to={'/posts'}>Go Posts</Link>
            </article>}
        </>)
}

const mapStateToProps = state => ({posts: state.post.posts})

export default connect(mapStateToProps)(Detail)