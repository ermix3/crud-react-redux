import {useState} from "react"
import {connect} from "react-redux"
import uuid from 'react-uuid';
import {useNavigate} from "react-router-dom";

import * as actions from "../../redux/actions";

const Add = ({addPost}) => {
    window.document.title = "Add Post"

    const [post, setPost] = useState({id: '', title: '', body: ''})

    const handleChange = ({target}) => setPost({...post, [target.name]: target.value})

    const navigate = useNavigate()

    const handleSubmit = e => {
        e.preventDefault()
        const id = uuid();
        addPost({...post, id})
        setPost({id: '', title: '', body: ''})

        navigate(`/posts/${id}`)
    }

    return (
        <>
            <h1>New Post</h1>
            <div>
                <form onSubmit={handleSubmit}>
                    <input type="text"
                           name="title"
                           value={post.title}
                           onChange={handleChange}
                           placeholder={'Enter The Title...'} required/><br/>

                    <textarea cols={20}
                              rows={10}
                              name="body"
                              value={post.body}
                              onChange={handleChange}
                              placeholder={'Enter Content...'} required/><br/>

                    <input type="submit" value="Add Post"/>
                </form>
            </div>
        </>
    )
}

const mapStateToProps = state => ({posts: state.post.posts})
const mapDispatchToProps = dispatch => ({addPost: post => dispatch(actions.add_post(post))})

export default connect(mapStateToProps, mapDispatchToProps)(Add)