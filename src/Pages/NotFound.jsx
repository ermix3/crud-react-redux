export default function NotFound() {
    window.document.title = "Not Found"

    return (
        <div>
            <h1 style={{color: 'red', fontSize: '500%'}}>Not Found :)</h1>
        </div>
    )
}