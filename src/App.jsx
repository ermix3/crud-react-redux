import {Route, Routes} from "react-router-dom";

import './App.css'

import Navbar from "./Components/Navbar";
import Home from "./Pages/Home";
import About from "./Pages/About";
import Post from "./Pages/posts/Posts.jsx";
import Edit from "./Pages/posts/Edit";
import NotFound from "./Pages/NotFound";
import Detail from "./Pages/posts/Detail.jsx";
import Add from "./Pages/posts/Add.jsx";

function App() {

    return (
        <div className="App">
            <Navbar/>
            <Routes>
                <Route path={'/'} element={<Home/>}/>
                <Route path={'/about'} element={<About/>}/>
                <Route path={'/posts'} element={<Post/>}/>
                <Route path={'/posts/:id'} element={<Detail/>}/>
                <Route path={'/posts/create'} element={<Add/>}/>
                <Route path={'/posts/edit/:id'} element={<Edit/>}/>
                <Route path="*" element={<NotFound/>}/>
            </Routes>
        </div>
    )
}

export default App
