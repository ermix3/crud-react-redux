import {Link} from "react-router-dom";

export default function Navbar() {
    return (
        <header>
            <nav>
                <ul style={{'display':'flex', 'listStyleType':'none' }}>
                    <li style={{'margin':'6px'}}><Link to="/">Home</Link></li>
                    <li style={{'margin':'6px'}}><Link to="/about">About</Link></li>
                    <li style={{'margin':'6px'}}><Link to="/posts">Posts</Link></li>
                    <li style={{'margin':'6px'}}><Link to="/posts/create">Add Post</Link></li>
                </ul>
            </nav>
        </header>
    )
}